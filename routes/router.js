const express = require('express');
const { isLoggedIn } = require('../util/auth-middleware');

function Router(models) {
    const router = express.Router();
    
    const mainController = require('../controllers/main').MainController(models);

    router.get('/course', mainController.getAllCourseData);
    
    router.get('/disciplinas', mainController.getAllDisciplinasData);
    
    router.post('/user', mainController.postNewUser);
    
    router.get('/saves', mainController.getAllSaves);
    
    router.post('/login', mainController.postLogin);
    
    router.get('/semestres', isLoggedIn, mainController.getAllUserSemestres);
    
    router.get('/current-disciplinas', isLoggedIn, mainController.getAllCurrentUserDisciplinas);
    
    router.get('/disciplinas-status', isLoggedIn, mainController.getCurrentUserDisciplinasStatus);
    
    router.get('/day-status', isLoggedIn, mainController.getDayStatus);
    
    router.get('/days-status', isLoggedIn, mainController.getDaysStatus);
    
    router.post('/lazer', isLoggedIn, mainController.postLazer);
    
    router.post('/estudar', isLoggedIn, mainController.postEstudar);
    
    router.post('/dormir', isLoggedIn, mainController.postDormir);

    return router;
}

exports.Router = Router;
