create database carreira;
CREATE USER carreira WITH ENCRYPTED PASSWORD 'carreira';
GRANT ALL PRIVILEGES ON DATABASE carreira TO carreira;
