exports.isLoggedIn = async (req, res, next) => {
  try {
    if (!req.user) {
      return res.status(403).json({ error: 'Usuário não autorizado.' });
    }
    const user = await User.findByPk(req.user.id);
    
    if (!user) {
      return res.status(403).json({ error: 'Usuário não autorizado.' });
    }
    next();
  } catch (err) {
    console.log(err);
  }
};
