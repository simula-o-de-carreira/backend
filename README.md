# Simulador de carreira

## Build

Nesse projeto iremos utilizar o node v10.16.3, essa versão específica pode ser instalada utilizando o [nvm](https://github.com/nvm-sh/nvm#installation-and-update)

Instale o gulp:

```
npm install -g gulp-cli
```

Em seguida instale as dependencias do projeto:

```
npm install
```

## Run

```
gulp
```
