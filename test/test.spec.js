const chai = require('chai');
const server = 'http://localhost:8080';
const test = require('../controllers/main');
const chaiHttp = require('chai-http');
const getAllCourseData = test.getAllCourseData;
const expect = chai.expect;

describe('Date', function () {
  const res = {
    status() {
      return this;
    },
    json() { }
  };

  it('getAllCourseData()', function (done) {
    let course;
    getAllCourseData({}, res, () => { })
      .then(function (newCourseData) {
        course = newCourseData;
        expect('Computação').to.be.equal(course[0].nome);
        done();
      })
  });

  it('getAllDisciplinasData()', (done) => {
    test.getAllDisciplinasData({}, res, () => { })
      .then((disciplinas) => {
        expect(disciplinas.length).to.be.equal(88);
        done();
      });
  });

  it('postNewUser()', function (done) {
    const req = {
      body: {
        name: "Teste de verificacao DB",
        birthday: "2000-09-11"
      }
    }
    test.postNewUser(req, res, () => { })
      .then(result => {
        expect(result.newUser.dataValues.name).to.be.equal(req.body.name);
        done();
      });
  });
  it('getAllSaves()', (done) => {
    test.getAllSaves({}, res, () => { })
      .then(users => {
        expect(users[0].id).to.be.equal(1);
        done();
      });
  });

  it('postLogin()', (done) => {
    const req = {
      body: {
        userId: 1
      }
    }
    test.postLogin(req, res, () => { })
      .then(token => {
        expect(token).to.be.not.equal(res.status(404).json({ error: 'Usuário não encontrado.' }));
        done();
      });
  });

  it('getAllUserSemestres()', (done) => {
    req = {
      user: {
        id: 1,
        name: "Usuário Teste",
        birthday: "1999-02-02T00:00:00.000Z",
        currentSemestre: 1,
        createdAt: "2019-11-25T22:30:53.616Z",
        updatedAt: "2019-11-25T22:30:53.616Z"
      }
    };
    test.getAllUserSemestres(req, res, () => { })
      .then(semestres => {
        expect(semestres[0].id).to.be.equal(1);
        done();
      });
  });

  // it('getAllCurrentUserDisciplinas()', (done) => {
  //   req = {
  //     user: {
  //       id: 1,
  //       name: "Usuário Teste",
  //       birthday: "1999-02-02T00:00:00.000Z",
  //       currentSemestre: 1,
  //       createdAt: "2019-11-25T22:30:53.616Z",
  //       updatedAt: "2019-11-25T22:30:53.616Z"
  //     }
  //   };
  //   test.getAllCurrentUserDisciplinas(req, res, () => { })
  //   .then(disciplinas => {
  //     console.log(disciplinas);
  //     expect(disciplinas[0].dataValues.id).to.be.equal(req.user.id);
  //     done();
  //   });
  // });

  it('getCurrentUserDisciplinasStatus()', done => {
    req = {
      user: {
        id: 1,
        name: "Usuário Teste",
        birthday: "1999-02-02T00:00:00.000Z",
        currentSemestre: 1,
        createdAt: "2019-11-25T22:30:53.616Z",
        updatedAt: "2019-11-25T22:30:53.616Z"
      }
    };
    test.getCurrentUserDisciplinasStatus(req, res, () => { })
      .then(currentDisc => {
        expect(currentDisc[4].dataValues.nome).to.be.equal('Prova 2 - Algorítmos e programação de computadores');
        done();
      });
  });

  it('getDayStatus()', (done) => {
    req = {
      user: {
        id: 1,
        name: "Usuário Teste",
        birthday: "1999-02-02T00:00:00.000Z",
        currentSemestre: 1,
        createdAt: "2019-11-25T22:30:53.616Z",
        updatedAt: "2019-11-25T22:30:53.616Z"
      }
    };
    test.getDayStatus(req, res, () => { })
      .then(dayStatus => {

        expect(dayStatus.dataValues.id).to.be.equal(1);
        done();
      });
  });

});

