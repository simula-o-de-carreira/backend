const { Model, DataTypes } = require('sequelize');

function Semestre(connection) {
  class SemestreModel extends Model {};
  return SemestreModel.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    semestre: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  }, { sequelize: connection, modelName: 'semestre' });
}

exports.Semestre = Semestre;
