const { Model, DataTypes } = require('sequelize');

function Curso(connection) {
  class CursoModel extends Model {};
  return CursoModel.init({
    nome: DataTypes.STRING,
    orgao: DataTypes.STRING,
    maxCrSemestre: DataTypes.INTEGER,
    minCrSemestre: DataTypes.INTEGER,
    permanenciaMin: DataTypes.INTEGER,
    permanenciaMax: DataTypes.INTEGER,
    crFormatura: DataTypes.INTEGER,
    crModuloLivre: DataTypes.INTEGER
  }, { sequelize: connection, modelName: 'curso' });
}

exports.Curso = Curso;
