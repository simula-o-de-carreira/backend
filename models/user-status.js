const { Model, DataTypes } = require('sequelize');

function UserStatus(connection) {
  class UserStatusModel extends Model {};
  return UserStatusModel.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    horasEstudadas: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    horasLazer: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    horasDormidas: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    diaAtual: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    multiplicadorDesempenho: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    }
  }, { sequelize: connection, modelName: 'userStatus' });
}

exports.UserStatus = UserStatus;