const { Model, DataTypes } = require('sequelize');

function Disciplina(connection) {
  class DisciplinaModel extends Model {};
  return DisciplinaModel.init({
    nome: DataTypes.STRING,
    orgao: DataTypes.STRING,
    qtnProvas: DataTypes.INTEGER,
    qntHorasProva: DataTypes.INTEGER,
    qtnTrabalhos: DataTypes.INTEGER,
    qntHorasTrabalho: DataTypes.INTEGER,
    cr: DataTypes.INTEGER
  }, { sequelize: connection, modelName: 'disciplina' });
}

exports.Disciplina = Disciplina;
