const { Model, DataTypes } = require('sequelize');

function DisciplinaItem(connection) {
  class DisciplinaItemModel extends Model {};
  return DisciplinaItemModel.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING,
      allowNull: false
    },
    horasEstudadas: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false
    },
    nota: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      allowNull: false
    }
  }, { sequelize: connection, modelName: 'disciplinaItem' });
}

exports.DisciplinaItem = DisciplinaItem;
