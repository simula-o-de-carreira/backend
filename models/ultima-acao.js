const { Model, DataTypes } = require('sequelize');

function UltimaAcao(connection) {
  class UltimaAcaoModel extends Model {};
  return UltimaAcaoModel.init({
    key: DataTypes.STRING,
    horasDeEstudo: DataTypes.FLOAT,
    horasDeLazer: DataTypes.FLOAT,
    horasDeSono:  DataTypes.FLOAT,
    pausa: DataTypes.BOOLEAN
  }, { sequelize: connection, modelName: 'ultimaacao' });
}

exports.UltimaAcao = UltimaAcao;
