const { Model, DataTypes } = require('sequelize');

function User(connection) {
  class UserModel extends Model {};
  return UserModel.init({
    name: DataTypes.STRING,
    matricula: DataTypes.STRING,
    birthday: DataTypes.DATE,
    currentSemestre: DataTypes.INTEGER,
    desempenho: DataTypes.FLOAT,
    cansaco: DataTypes.FLOAT
  }, { sequelize: connection, modelName: 'user' });
}

exports.User = User;
