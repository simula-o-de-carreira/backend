const { Model, DataTypes } = require('sequelize');

function DesempenhoDiario(connection) {
  class DesempenhoDiarioModel extends Model {};
  return DesempenhoDiarioModel.init({
    desempenho: DataTypes.FLOAT
  }, { sequelize: connection, modelName: 'desempenhodiario' });
}

exports.DesempenhoDiario = DesempenhoDiario;
