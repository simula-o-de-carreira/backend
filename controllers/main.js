const jwt = require('jsonwebtoken');

exports.MainController = function(models) {

  async function addDisciplinaToSemestre(semestre, disciplina) {
    let contProvas = 0;
    try {
      while (contProvas < disciplina.qtnProvas) {
        await models.DisciplinaItem.create({
          semestreId: semestre.id,
          disciplinaId: disciplina.id,
          nome: `Prova ${contProvas + 1} - ${disciplina.nome}`,
        });
        contProvas++;
      }
  
      let contTrabalhos = 0;
      while (contTrabalhos < disciplina.qtnTrabalhos) {
        await models.DisciplinaItem.create({
          semestreId: semestre.id,
          disciplinaId: disciplina.id,
          nome: `Trabalho ${contTrabalhos + 1} - ${disciplina.nome}`,
        });
        contTrabalhos++;
      }
    } catch (err) {
      console.log(err);
    }
  }
  
  async function findUserDisciplinas(disciplinaItems, cb) {
    let disciplinas = [];
    let cont = 0;
  
    while (cont < disciplinaItems.length) {
      const disciplina = await models.Disciplina.findByPk(
        disciplinaItems[cont].disciplinaId
      );
  
      const fetcheDisciplina = disciplinas.find(element => {
        return element.id === disciplina.id;
      });
      if (!fetcheDisciplina) {
        await disciplinas.push(disciplina);
      }
      ++cont;
    }
  
    cb(disciplinas);
  }
  
  async function getLastDayStatus(userId, cb) {
    try {
      const dayStatus = await models.DayStatus.findOne({
        where: {
          userId: userId
        },
        order: [['createdAt', 'DESC']]
      });
      cb(dayStatus);
    } catch (err) {
      console.log(err);
    }
  }

  const Controllers = {
    getAllCourseData: async (req, res, next) => {
      const courseData = await models.Course.findAll();
      res.status(200).json(courseData);
      return courseData;
    },
    getAllDisciplinasData: async (req, res, next) => {
      const disciplinas = await models.Disciplina.findAll();
      res.status(200).json(disciplinas);
      return disciplinas;
    },
    postNewUser: async (req, res, next) => {
      const DayStatus = models.UserStatus;
      try {
        const { name, birthday } = req.body;
        const FDC = await models.Disciplina.findOne({ where: { nome: 'Formação docente em computação' } });
        const OEB = await models.Disciplina.findOne({ where: { nome: 'Organização da educação brasileira' } });
        const APC = await models.Disciplina.findOne({ where: { nome: 'Algorítmos e programação de computadores' } });
    
        const newUser = await models.User.create({
          name: name,
          birthday: new Date(birthday).toISOString(),
          currentSemestre: 1
        });
        const newUserSemestre = await models.Semestre.create({
          semestre: 1,
          userId: newUser.id
        });
        await DayStatus.create({
          userId: newUser.id
        });
        addDisciplinaToSemestre(newUserSemestre, FDC);
        addDisciplinaToSemestre(newUserSemestre, OEB);
        addDisciplinaToSemestre(newUserSemestre, APC);
    
        res.status(201).json(newUser);
        return {
          newUser,
          newUserSemestre
        }
      } catch (err) {
        console.log(err);
      }
    },
    getAllSaves: async (req, res, next) => {
      const users = await models.User.findAll();
      res.status(200).json(users);
      return users;
    },
    postLogin: async (req, res, next) => {
      const { userId } = req.body;
    
      try {
        const user = await models.User.findByPk(userId);
    
        if (!user) {
          return res.status(404).json({ error: 'Usuário não encontrado.' });
        }
    
        const token = jwt.sign(
          { user: user },
          'user_data_secret',
          { expiresIn: '5h' }
        );
    
        res.status(201).json(token);
        return token;
      } catch (err) {
        console.log(err);
      }
    },
    getAllUserSemestres: async (req, res, next) => {
      const user = req.user;
      const semestres = await models.Semestre.findAll({ where: { userId: user.id } });
      res.status(200).json(semestres);
      return semestres;
    },
    getAllCurrentUserDisciplinas: async (req, res, next) => {
      try {
        const user = req.user;
        const userSemestre = await models.Semestre.findOne({
          where: {
            semestre: user.currentSemestre,
            userId: user.id
          }
        });
    
        const disciplinasItem = await models.DisciplinaItem.findAll({
          where: {
            semestreId: userSemestre.id
          }
        });
    
        let retTest;
        
        findUserDisciplinas(disciplinasItem, disciplinas => {
          res.status(200).json(disciplinas);
          retTest = disciplinas;
        });
        
        return retTest;
    
      } catch (err) {
        console.log(err);
      }
    },
    getCurrentUserDisciplinasStatus: async (req, res, next) => {
      const user = req.user;
      const userSemestre = await models.Semestre.findOne({
        where: {
          semestre: user.currentSemestre,
          userId: user.id
        }
      });
    
      const disciplinas = await models.DisciplinaItem.findAll({
        where: {
          semestreId: userSemestre.id
        }
      });
    
      res.status(200).json(disciplinas);
      return disciplinas;
    },
    getDayStatus: async (req, res, next) => {
      const DayStatus = models.UserStatus;
      const dayStatus = await DayStatus.findOne({
        where: {
          userId: req.user.id
        }
      });
      res.status(200).json(dayStatus);
      return dayStatus;
    },
    getDaysStatus: async (req, res, next) => {
      const DayStatus = models.UserStatus;
      const daysStatus = await DayStatus.findAll({
        where: {
          userId: req.user.id
        }
      });
      res.status(200).json(daysStatus);
    },
    postLazer: (req, res, next) => {
      const { hoursQnt } = req.body;
      getLastDayStatus(req.user.id, async (dayStatus) => {
        dayStatus.horasLazer += hoursQnt;
        await dayStatus.save();
    
        res.status(200).json(dayStatus);
      });
    },
    postEstudar: async (req, res, next) => {
      const { hoursQnt, taskName } = req.body;
    
      const disciplinaItem = await models.DisciplinaItem.findOne({
        where: {
          nome: taskName,
          semestreId: req.user.currentSemestre
        }
      });
      disciplinaItem.horasEstudadas += hoursQnt;
      await disciplinaItem.save();
        
      getLastDayStatus(req.user.id, async (dayStatus) => {
        dayStatus.horasEstudadas += hoursQnt;
        await dayStatus.save();
    
        res.status(200).json({ dia: dayStatus, disciplina: disciplinaItem });
      });
    },
    postDormir: (req, res, next) => {
      const { hoursQnt } = req.body;
      getLastDayStatus(req.user.id, async (dayStatus) => {
        dayStatus.horasDormidas += hoursQnt;
        await dayStatus.save();
    
        res.status(200).json(dayStatus);
      });
    }
  };
  return Controllers;
}
