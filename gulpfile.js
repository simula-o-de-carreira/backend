'use strict';

const { series, parallel, src, dest, watch } = require('gulp');
const spawn = require('child_process').spawn;

const watchedFiles = [
    'index.js', 
    'controllers', 
    'routes',
    'util',
    'models'
];

var node;

function startServer(cb) {
  if (node) node.kill();
  node = spawn('node', ['index.js'], { stdio: 'inherit' });
  node.on('close', function(code) {
    if (code === 8) {
      console.log('Erro, esperando mudanças no arquivo.');
    }
  });
}

function watchServer() {
  startServer();
  watch(watchedFiles, function respanwServer(cb) {
    startServer();
    cb();
  });
}

exports.default = watchServer;
