'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const { Sequelize } = require('sequelize');
const fs = require('fs');
const jwt = require('jsonwebtoken');

// Para utilizar uma porta diferente da 8080, simplesmente exporte a variável PORT
// com o número da porta desejada. EX:

// export PORT 10000
// node index.js

// para usar um banco de dados persistente o app deve ser colocado em modo de produção
// EX:

// export APP_ENV="production"
// export APP_DB_URL="postgres://carreira:carreira@127.0.0.1:5432/carreira"
// node index.js

var APP_PORT = 8080;
var APP_ENV = 'development';
var APP_DB_URL = null;

if (process.env.APP_PORT)
  PORT = Number.parseInt(process.env.APP_PORT);

if (process.env.APP_ENV)
  APP_ENV = process.env.APP_ENV;

if (APP_ENV == 'development') {
  if (process.env.APP_DB_URL)
    APP_DB_URL = process.env.APP_DB_URL;
  else
    APP_DB_URL = 'sqlite::memory:'
}

if (APP_ENV == 'production') {
  if (process.env.APP_DB_URL)
    APP_DB_URL = process.env.APP_DB_URL;
  else {
    console.error("A URL de conexão com o banco de dados deve ser definida utilizando a variável de ambiente APP_DB_URL.");
    process.exit();
  }
}

const app = express();
const dbConn = new Sequelize(APP_DB_URL);

const Persistence = {
  User: require('./models/user').User(dbConn),
  Disciplina: require('./models/disciplina').Disciplina(dbConn),
  Curso: require('./models/curso').Curso(dbConn),
  Semestre: require('./models/semestre').Semestre(dbConn),
  DisciplinaItem: require('./models/disciplina-item').DisciplinaItem(dbConn),
  UserStatus: require('./models/user-status').UserStatus(dbConn),
  DesempenhoDiario: require('./models/desempenho-diario').DesempenhoDiario(dbConn),
  UltimaAcao: require('./models/ultima-acao').UltimaAcao(dbConn)
};

const routes = require('./routes/router').Router(Persistence);

app.use(bodyParser.json());

// -- Middleware de decodificação do JWT --
app.use((req, res, next) => {
  const authHeader = req.get('Authorization');

  if (!authHeader) {
    return next();
  }

  try {
    const token = authHeader.split(' ')[1];
    let decodedToken;

    decodedToken = jwt.verify(token, 'user_data_secret');
    req.user = decodedToken.user;
  } catch (err) {
    console.log(err);
  }
  
  next();
});

app.use('/', routes);

// -- Relações do DB --
Persistence.Curso.hasMany(Persistence.Disciplina);
Persistence.User.hasMany(Persistence.UserStatus);
Persistence.User.hasMany(Persistence.Semestre);
Persistence.User.hasMany(Persistence.DesempenhoDiario);
Persistence.User.hasMany(Persistence.UltimaAcao);
Persistence.Semestre.hasMany(Persistence.Disciplina);
Persistence.Semestre.hasMany(Persistence.DisciplinaItem);
Persistence.Disciplina.hasMany(Persistence.DisciplinaItem);
Persistence.User.belongsToMany(Persistence.Disciplina, {through: "MatriculaEmDisciplinas"});
Persistence.Disciplina.belongsToMany(Persistence.User, {through: "MatriculaEmDisciplinas"});
Persistence.User.belongsToMany(Persistence.Disciplina, {through: "AprovadoDisciplina"});
Persistence.Disciplina.belongsToMany(Persistence.User, {through: "AprovadoDisciplina"});

dbConn
  .sync() // .sync({ force: true }) // Force: true -> dropa o DB atual
  .then(function () {
    fs.readFile('./util/computacao.json', 'utf-8', function (err, data) {
      if (err) {
        throw err;
      }

      var disciplinasJson = JSON.parse(data);
      var curso = disciplinasJson.curso;
      var disciplinasFluxo = disciplinasJson.curso.disciplinasFluxo;

      Persistence.Curso
        .findOrCreate({
          where: {
            nome: curso.nome
          },
          defaults: {
            orgao: curso.orgao,
            maxCrSemestre: curso.maxCrSemestre,
            minCrSemestre: curso.minCrSemestre,
            permanenciaMin: curso.permanenciaMin,
            permanenciaMax: curso.permanenciaMax,
            crFormatura: curso.crFormatura,
            crModuloLivre: curso.crModuloLivre
          }
        })
        .then(function ([curso, created]) {
          if (created) {
            disciplinasFluxo.forEach(function (disciplina) {
              Persistence.Disciplina.create({
                nome: disciplina.nome,
                qtnProvas: disciplina.qtnProvas,
                qntHorasProva: disciplina.qntHorasProva,
                qtnTrabalhos: disciplina.qtnTrabalhos,
                qntHorasTrabalho: disciplina.qntHorasTrabalho,
                orgao: disciplina.orgao,
                cr: disciplina.cr,
                cursoId: curso.get('id')
              });
            });
          }
          app.listen(APP_PORT, function () {
            console.log('Carreira app listening on port ' + APP_PORT);
          });
        })
        .catch(function (err) {
          throw err;
        });
    });
  });
